export default function Logo({ type = "default" }) {
    if (type === "muted") {
        return (<img src="images/logo-muted.svg" alt="muted logo"></img>);
    }
    else {
        return (<img src="images/logo.svg" alt="unmuted logo"></img>);
    }
}