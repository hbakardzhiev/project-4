import { Button, ButtonGroup, Grid } from "@mui/material";
import Logo from "../components/logo/Logo";
import TextField from '@mui/material/TextField';

export default function Header() {
    return (
        <Grid direction="row" justifyContent="center" alignItems="center" container spacing={1} margin={0.1} wrap='nowrap'>
            <Grid item>
                <Logo />
            </Grid >
            <Grid item xs={5}>
                <TextField fullWidth label="Find items, users, and activities" variant="outlined" />
            </Grid>
            <Grid item>
                <ButtonGroup>
                    <Button>Home</Button>
                    <Button>Activity</Button>
                    <Button>Explore</Button>
                </ButtonGroup>
            </Grid>
        </Grid>
    )
} 